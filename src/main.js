import "core-js/stable";
import "@/utils/crypto/crypto.js";
import "regenerator-runtime/runtime";
import Vue from "vue";
import App from "./App";
import store from "./store";
import router from "./router";
import "./plugins";

import './assets/fonts/iconfont.css';

// rem适配
import './utils/rem';

import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
import ECharts from 'vue-echarts';
import 'echarts/lib/chart/bar';
import "echarts/lib/component/legend";
Vue.component('v-chart', ECharts);
// 复制插件
import Clipboard from 'v-clipboard';
Vue.use(Clipboard);

// 数字动画跳动
import VueCountUp from 'vue-countupjs';
Vue.use(VueCountUp);

import FileUpload from "@/components/Qy/FileUpload";
Vue.component('uploadImg', FileUpload);

Vue.config.productionTip = false;

import directives from '@/utils/directives';
Vue.use(directives);

new Vue({
    el: "#qy-vue-admin",
    router,
    store,
    render: (h) => h(App),
});
import draggable from 'vuedraggable';
Vue.component('draggable', draggable);

import {
    quillEditor
} from 'vue-quill-editor';
Vue.component('quillEditor', quillEditor)

//地图插件
import BaiduMap from 'vue-baidu-map';
Vue.use(BaiduMap, {    
    ak: 'tjttSndyZQz2Ofoi9ftaal6GaiKteSzy'
});
import Map from "@/components/Qy/map";
Vue.component('mapaddress', Map);
//图片插件
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import utils from '@/utils/index';
Vue.prototype.$utils = utils;

import io from "@/utils/scoket/socketio.js";
import { encode, decode } from 'js-base64';
var socket = io("https://pushserve.youdiy.cn:2120/");
Vue.prototype.$utils.socket = function() {
    socket = socket.disconnected ? io("https://pushserve.youdiy.cn:2120/") : socket;
    socket.on("connect", (res) => {
        socket.emit("login", 'S_' + localStorage.getItem('acid') || '');
    });
    // 后端推送来消息时
    socket.on("new_msg", function(msg) {
        store.dispatch('message/setNewMessage', false);
        try {
            let jiemi = decode(msg);
            jiemi = JSON.parse(jiemi);
            console.log(jiemi, 'jiemi')
            if (jiemi.account_type == 3) {
                store.dispatch('message/setNewMessage', true);
                store.dispatch('message/setMessage', jiemi);
            } else if (jiemi.account_type == 2) {
                store.dispatch('message/setNewMessage', true);
                store.dispatch('message/setMessage', jiemi);
                console.log(store.getters['message/message'], 'getters')
            } else if (jiemi.form_user_id) {
                store.dispatch('message/setNewMessage', true);
                store.dispatch('message/setMessage', jiemi);
            };
        } catch (e) {

        };
    });
    // 后端推送来在线数据时
    socket.on("update_online_count", function(online_stat) {});
};
Vue.prototype.$utils.disconnect = function() {
    socket.disconnect((e) => {});
};

Vue.use(Viewer);
Viewer.setDefaults({
    Options: {
        "inline": true,
        "button": true,
        "navbar": true,
        "title": true,
        "toolbar": true,
        "tooltip": true,
        "movable": true,
        "zoomable": true,
        "rotatable": true,
        "scalable": true,
        "transition": true,
        "fullscreen": true,
        "keyboard": true,
        "url": "data-source"
    }
});
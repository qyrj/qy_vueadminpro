const state = {
    message: '',
    newMessage: false,
    offlineMessage: false,
    messageNum: 0,
    systemMessage: '',
};

const getters = {
    message(state) {
        return state.message;
    },
    newMessage(state) {
        return state.newMessage;
    },
    offlineMessage(state) {
        return state.offlineMessage;
    },
    messageNum(state) {
        return state.messageNum;
    },
    systemMessage(state) {
        return state.systemMessage;
    },
};

const mutations = {
    setMessage(state, info) {
        state.message = info;
    },
    setNewMessage(state, info) {
        state.newMessage = info;
    },
    setOfflineMessage(state, info) {
        state.offlineMessage = info;
    },
    addMessageNum(state, info) {
        state.messageNum += info;
    },
    minusMessageNum(state, info) {
        state.messageNum -= info;
    },
    setMessageNum(state, info) {
        state.messageNum = info;
    },
    setSystemMessage(state, info) {
        state.systemMessage = info;
    },
};

const actions = {
    setMessage({ commit }, info) {
        commit('setMessage', info);
    },
    setNewMessage({ commit }, info) {
        commit('setNewMessage', info);
    },
    setOfflineMessage({ commit }, info) {
        commit('setOfflineMessage', info);
    },
    addMessageNum({ commit }, info) {
        commit('addMessageNum', info);
    },
    minusMessageNum({ commit }, info) {
        commit('minusMessageNum', info);
    },
    setMessageNum({ commit }, info) {
        commit('setMessageNum', info);
    },
    setSystemMessage({ commit }, info) {
        commit('setSystemMessage', info);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
}
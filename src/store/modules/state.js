const state = {
    category_list: '', //图片管理组件，图片分类信息
    file_list: '', //图片管理组件，文件列表
    area_arr: '', //区域列表
};
const getters = {
    category_list: (state) => state.category_list,
    file_list: (state) => state.file_list,
    area_arr: (state) => state.area_arr,
};
const mutations = {
    // 设置图片分类信息
    setCategoryList(state, info) {
        state.category_list = info;
    },
    setFileList(state, info) {
        state.file_list = info;
    },
    setAreaList(state, info) {
        state.area_arr = info;
    }
};
const actions = {
    // 设置图片组件分类信息
    setCategoryList({ commit }, info) {
        commit('setCategoryList', info)
    },
    // 设置图片组件文件列表
    setFileList({ commit }, info) {
        commit('setFileList', info)
    },
    // 设置图片组件文件列表
    setAreaList({ commit }, info) {
        commit('setAreaList', info)
    },
};
export default { state, getters, mutations, actions };
/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 格式化时间
 * @param time
 * @param cFormat
 * @returns {string|null}
 */
import { MessageBox } from "element-ui";
export function parseTime(time, cFormat) {
    if (arguments.length === 0) {
        return null;
    }
    const format = cFormat || "{y}-{m}-{d} {h}:{i}:{s}";
    let date;
    if (typeof time === "object") {
        date = time;
    } else {
        if (typeof time === "string" && /^[0-9]+$/.test(time)) {
            time = parseInt(time);
        }
        if (typeof time === "number" && time.toString().length === 10) {
            time = time * 1000;
        }
        date = new Date(time);
    }
    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
        s: date.getSeconds(),
        a: date.getDay(),
    };
    const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
        let value = formatObj[key];
        if (key === "a") {
            return ["日", "一", "二", "三", "四", "五", "六"][value];
        }
        if (result.length > 0 && value < 10) {
            value = "0" + value;
        }
        return value || 0;
    });
    return time_str;
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 格式化时间
 * @param time
 * @param option
 * @returns {string}
 */
export function formatTime(time, option) {
    if (("" + time).length === 10) {
        time = parseInt(time) * 1000;
    } else {
        time = +time;
    }
    const d = new Date(time);
    const now = Date.now();

    const diff = (now - d) / 1000;

    if (diff < 30) {
        return "刚刚";
    } else if (diff < 3600) {
        // less 1 hour
        return Math.ceil(diff / 60) + "分钟前";
    } else if (diff < 3600 * 24) {
        return Math.ceil(diff / 3600) + "小时前";
    } else if (diff < 3600 * 24 * 2) {
        return "1天前";
    }
    if (option) {
        return parseTime(time, option);
    } else {
        return (
            d.getMonth() +
            1 +
            "月" +
            d.getDate() +
            "日" +
            d.getHours() +
            "时" +
            d.getMinutes() +
            "分"
        );
    }
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 将url请求参数转为json格式
 * @param url
 * @returns {{}|any}
 */
export function paramObj(url) {
    const search = url.split("?")[1];
    if (!search) {
        return {};
    }
    return JSON.parse(
        '{"' +
        decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, " ") +
        '"}'
    );
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 父子关系的数组转换成树形结构数据
 * @param data
 * @returns {*}
 */
export function translateDataToTree(data) {
    const parent = data.filter(
        (value) => value.parentId === "undefined" || value.parentId == null
    );
    const children = data.filter(
        (value) => value.parentId !== "undefined" && value.parentId != null
    );
    const translator = (parent, children) => {
        parent.forEach((parent) => {
            children.forEach((current, index) => {
                if (current.parentId === parent.id) {
                    const temp = JSON.parse(JSON.stringify(children));
                    temp.splice(index, 1);
                    translator([current], temp);
                    typeof parent.children !== "undefined" ?
                        parent.children.push(current) :
                        (parent.children = [current]);
                }
            });
        });
    };
    translator(parent, children);
    return parent;
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 树形结构数据转换成父子关系的数组
 * @param data
 * @returns {[]}
 */
export function translateTreeToData(data) {
    const result = [];
    data.forEach((item) => {
        const loop = (data) => {
            result.push({
                id: data.id,
                name: data.name,
                parentId: data.parentId,
            });
            const child = data.children;
            if (child) {
                for (let i = 0; i < child.length; i++) {
                    loop(child[i]);
                }
            }
        };
        loop(item);
    });
    return result;
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 10位时间戳转换
 * @param time
 * @returns {string}
 */
export function tenBitTimestamp(time) {
    const date = new Date(time * 1000);
    const y = date.getFullYear();
    let m = date.getMonth() + 1;
    m = m < 10 ? "" + m : m;
    let d = date.getDate();
    d = d < 10 ? "" + d : d;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let minute = date.getMinutes();
    let second = date.getSeconds();
    minute = minute < 10 ? "0" + minute : minute;
    second = second < 10 ? "0" + second : second;
    return y + "年" + m + "月" + d + "日" + h + ":" + minute + ":" + second; //组合
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 13位时间戳转换
 * @param time
 * @returns {string}
 */
export function thirteenBitTimestamp(time) {
    const date = new Date(time / 1);
    const y = date.getFullYear();
    let m = date.getMonth() + 1;
    m = m < 10 ? "" + m : m;
    let d = date.getDate();
    d = d < 10 ? "" + d : d;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let minute = date.getMinutes();
    let second = date.getSeconds();
    minute = minute < 10 ? "0" + minute : minute;
    second = second < 10 ? "0" + second : second;
    return y + "年" + m + "月" + d + "日 " + h + ":" + minute + ":" + second; //组合
}
export function formatDate(time, format = 'y-m-d') {
    const date = new Date(time);
    const y = date.getFullYear();
    let m = date.getMonth() + 1;
    m = m < 10 ? "0" + m : m;
    let d = date.getDate();
    d = d < 10 ? "0" + d : d;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let minute = date.getMinutes();
    let second = date.getSeconds();
    minute = minute < 10 ? "0" + minute : minute;
    second = second < 10 ? "0" + second : second;
    let str = '';
    if (format === 'y-m-d h:m') {
        str = `${y}-${m}-${d} ${h}:${minute}`;
    } else if (format === 'y-m-d h:m:s') {
        str = `${y}-${m}-${d} ${h}:${minute}:${second}`;
    } else if (format === 'y-m-d') {
        str = y + "-" + m + "-" + d;
    } else if (format === 'm-d') {
        str = m + '-' + d;
    }
    return str; //组合
}
/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 获取随机id
 * @param length
 * @returns {string}
 */
export function uuid(length = 32) {
    const num = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    let str = "";
    for (let i = 0; i < length; i++) {
        str += num.charAt(Math.floor(Math.random() * num.length));
    }
    return str;
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description m到n的随机数
 * @param m
 * @param n
 * @returns {number}
 */
export function random(m, n) {
    return Math.floor(Math.random() * (m - n) + n);
}

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description addEventListener
 * @type {function(...[*]=)}
 */
export const on = (function() {
    return function(element, event, handler, useCapture = false) {
        if (element && event && handler) {
            element.addEventListener(event, handler, useCapture);
        }
    };
})();

/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description removeEventListener
 * @type {function(...[*]=)}
 */
export const off = (function() {
    return function(element, event, handler, useCapture = false) {
        if (element && event) {
            element.removeEventListener(event, handler, useCapture);
        }
    };
})();

export const messageBox = function(content = '网络异常', title = '提示', resolve = () => {}, reject = () => {}) {
    return MessageBox.confirm(content, title, {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        resolve()
    }).catch(() => {
        reject()
    });
}

function getType(obj) {
    //tostring会返回对应不同的标签的构造函数
    var toString = Object.prototype.toString;
    var map = {
        "[object Boolean]": "boolean",
        "[object Number]": "number",
        "[object String]": "string",
        "[object Function]": "function",
        "[object Array]": "array",
        "[object Date]": "date",
        "[object RegExp]": "regExp",
        "[object Undefined]": "undefined",
        "[object Null]": "null",
        "[object Object]": "object",
    };
    if (obj instanceof Element) {
        return "element";
    }
    return map[toString.call(obj)];
};

function deepClone(data) {
    var type = getType(data);
    var obj;
    if (type === "array") {
        obj = [];
    } else if (type === "object") {
        obj = {};
    } else {
        //不再具有下一层次
        return data;
    }
    if (type === "array") {
        for (var i = 0, len = data.length; i < len; i++) {
            obj.push(this.deepClone(data[i]));
        }
    } else if (type === "object") {
        for (var key in data) {
            obj[key] = this.deepClone(data[key]);
        }
    }
    return obj;
};
//校验经纬度的格式是否符合规范
function verifylonglat(rule, value, callback) {
    let val = value.split(',');
    if (val.length < 2) {
        callback(new Error("经纬度例:(26.076521,119.301910)英文字符逗号隔开"));
    } else {
        let longreg = /^(\-|\+)?(((\d|[1-9]\d|1[0-7]\d|0{1,3})\.\d{0,6})|(\d|[1-9]\d|1[0-7]\d|0{1,3})|180\.0{0,6}|180)$/;
        let latreg = /^(\-|\+)?([0-8]?\d{1}\.\d{0,6}|90\.0{0,6}|[0-8]?\d{1}|90)$/;
        let flag = longreg.test(val[1]) && latreg.test(val[0]);
        if (!flag) {
            callback(new Error("经纬度例:(26.076521,119.301910)英文字符逗号隔开"));
        } else {
            callback();
        }
    }
};
// 打开地址选取链接
export function openMap() {
    window.open('https://lbs.qq.com/tool/getpoint/');
};
export function getWeek(time) {
    if (!time) { return };
    let day = +new Date(time).getDay();
    let week;
    switch (day) {
        case 0:
            week = '日';
            break;
        case 1:
            week = '一';
            break;
        case 2:
            week = '二';
            break;
        case 3:
            week = '三';
            break;
        case 4:
            week = '四';
            break;
        case 5:
            week = '五';
            break;
        case 6:
            week = '六';
            break;
        default:
            break;
    }
    return week;
};
export function clientHeight() {
    return document.body.clientHeight;
};
export function tableHeight() {
    let height = clientHeight() > 0 ? (clientHeight() - 315) : 620
    return height;
};

function activityStatusVal(status = '1') {
    let statusType = {
        "1": "报名中",
        "2": "活动中",
        "3": "报名截止",
    };
    return statusType[status];
};

function payTypeName(status = '1') {
    let statusType = {
        "1": "微信支付",
        "2": "钱包支付",
    };
    return statusType[status];
};

function activityOrderStatus(status = '1') {
    let statusType = {
        "-1": "已取消",
        "0": "待支付",
        "1": "待核销",
        "2": "已核销",
    };
    return statusType[status];
};

function staticUrl(url) {
    return 'https://chazaiduancdn.youdiy.cn/' + url;
};
export default {
    deepClone,
    verifylonglat,
    formatDate,
    getWeek,
    openMap,
    clientHeight,
    tableHeight,
    payTypeName,
    activityStatusVal,
    activityOrderStatus,
    staticUrl
}
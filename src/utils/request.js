import axios from "axios";
import {
    baseURL,
    contentType,
    invalidCode,
    messageDuration,
    noPermissionCode,
    requestTimeout,
    successCode,
    tokenName,
    debounce,
} from "@/config/settings";
import { messageBox } from "./index";
import { Loading, Message } from "element-ui";
import store from "@/store";
import qs from "qs";
import router from "@/router";
import _ from "lodash";
let BaseURL = process.env.NODE_ENV !== "production" ? baseURL : window.location.origin + '/index.php?s=';
const service = axios.create({
    baseURL: BaseURL,
    timeout: requestTimeout,
    headers: {
        "Content-Type": contentType,
    }
});
let loadingInstance;
service.interceptors.request.use(
    (config) => {
        // if (store.getters["user/accessToken"]) {
        //   config.headers[tokenName] = store.getters["user/accessToken"];
        // }
        // if (config.data) {
        //   config.data = _.pickBy(config.data, _.identity);
        // }
        if (process.env.NODE_ENV !== "test") {
            if (contentType === "application/x-www-form-urlencoded;charset=UTF-8") {
                if (config.data && !config.data.param) {
                    config.data = qs.stringify(config.data);
                }
            }
        }
        const needLoading = () => {
            let status = false;
            debounce.forEach((item) => {
                if (_.includes(config.url, item)) {
                    status = true;
                }
            });
            return status;
        };
        if (needLoading()) {
            loadingInstance = Loading.service({ background: 'rgba(0,0,0,0)' });
        }

        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

const errorMsg = (message) => {
    return Message({
        message: message,
        type: "error",
        duration: messageDuration,
    });
};

service.interceptors.response.use(
    (response) => {
        if (loadingInstance) {
            loadingInstance.close();
        }
        const { status, data, config } = response;
        const { code, msg } = data;
        if (code !== successCode && code !== 0) {
            switch (code) {
                case invalidCode:
                    errorMsg(msg || `后端接口${code}异常`);
                    store.dispatch("user/resetAccessToken");
                    if (localStorage.getItem('login_type') == 4) { //代理商账号
                        router.push(`/agent/login?acid=${localStorage.getItem('acid') || 1}`);
                    } else if (localStorage.getItem('login_type') == 5) { //平台
                        router.push(`/platform/login`);
                    } else if (localStorage.getItem('login_type') == 6) { //平台子账号
                        router.push(`/platformSonAccount/login?acid=${localStorage.getItem('acid') || 1}`);
                    } else if (localStorage.getItem('login_type') == 7) { //sass子账号
                        router.push(`/sassAccount/login?acid=${localStorage.getItem('acid') || 1}`);
                    } else {
                        router.push(`/platform/login`);
                    }
                    break;
                case noPermissionCode:
                    router.push({
                        path: "/401",
                    });
                    break;
                default:
                    errorMsg(msg || `后端接口${code}异常`);
                    break;
            }
            return Promise.reject(
                "qy-vue-admin请求异常拦截:" +
                JSON.stringify({ url: config.url, code, msg }) || "Error"
            );
        } else {
            return data;
        }
    },
    (error) => {
        if (loadingInstance) {
            loadingInstance.close();
        }
        /*网络连接过程异常处理*/
        let { message } = error;
        switch (message) {
            case "Network Error":
                message = "网络异常,请联系管理员";
                break;
            case "timeout of 5000ms exceeded":
                message = "连接超时,请联系管理员";
                break;
            case "Request failed with status code":
                message = "连接" + message.substr(message.length - 3) + "异常,请联系管理员";
                break;
        }
        errorMsg(message || "网络异常");
        messageBox('网络异常，点击确定刷新页面！', '提示', () => {
            window.location.reload()
        });
        return Promise.reject(error);
    }
);
export default service;
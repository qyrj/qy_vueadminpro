import { Heading, Img, Keel, Text } from "qy-icon-pro/pfh-keel";
import "@/styles/keel-variables.scss";
const VabKeel = Keel;
const VabKeelHeading = Heading;
const VabKeelImg = Img;
const VabKeelText = Text;
export { VabKeel, VabKeelHeading, VabKeelImg, VabKeelText };

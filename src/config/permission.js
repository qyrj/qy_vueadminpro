/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description 路由守卫，目前两种模式：all模式与intelligence模式
 */
import router from "../router";
import store from "../store";
import VabProgress from "nprogress";
import "nprogress/nprogress.css";
import getPageTitle from "@/utils/pageTitle";
import {
    authentication,
    loginInterception,
    routesWhiteList,
    progressBar,
} from "@/config/settings";

VabProgress.configure({
    easing: "ease",
    speed: 500,
    trickleSpeed: 200,
    showSpinner: false,
});
router.beforeResolve(async(to, from, next) => {
    if (progressBar) VabProgress.start();
    let hasToken = store.getters["user/accessToken"];
    if (!loginInterception) hasToken = true;
    if (hasToken) {
        if (to.path === '/store/login') {
            next({ path: "/" });
            if (progressBar) VabProgress.done();
        } else {
            const hasPermissions =
                store.getters["user/permissions"] &&
                store.getters["user/permissions"].length > 0;
            if (hasPermissions) {
                next();
            } else {
                try {
                    const permissions = await store.dispatch("user/getInfo");
                    let accessRoutes = [];
                    if (authentication === "intelligence") {
                        accessRoutes = await store.dispatch(
                            "routes/setRoutes",
                            permissions
                        );
                    } else if (authentication === "all") {
                        accessRoutes = await store.dispatch("routes/setAllRoutes");
                    };
                    router.addRoutes(accessRoutes);
                    if (from.path === "/admin_login" || from.path === "/store/login" || from.path === "/agent/login" || from.path === "/platform/login" || from.path === "/platformSonAccount/login") {
                        if (accessRoutes[0].children && accessRoutes[0].children.length > 0) {
                            let childrenUrl = accessRoutes[0].children.filter(p => {
                                return p.hidden == 0 || !p.hidden;
                            });
                            if (childrenUrl[0]) {
                                next({ path: childrenUrl[0].path });
                            } else {
                                next({ path: accessRoutes[0].path });
                            };
                        } else {
                            next({ path: accessRoutes[0].path });
                        }
                    } else {
                        next({...to, replace: true });
                    }
                } catch (error) {
                    await store.dispatch("user/resetAccessToken");
                    // next(`/login?redirect=${to.path}`);
                    next(`/platform/login`);
                    if (progressBar) VabProgress.done();
                }
            }
        }
    } else {
        if (routesWhiteList.indexOf(to.path) !== -1) {
            next();
        } else {
            // next(`/login?redirect=${to.path}`);
            next(`/platform/login`);
            if (progressBar) VabProgress.done();
        }
    }
    document.title = getPageTitle(to.meta.title);
});
router.afterEach(() => {
    if (progressBar) VabProgress.done();
});
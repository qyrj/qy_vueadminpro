/**
 * @copyright qy-vue-admin 1778486252@qq.com
 * @description router全局配置，如有必要可分文件抽离
 */

import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/layouts";
import { routerMode } from "@/config/settings";

Vue.use(VueRouter);

export const constantRoutes = [{
        path: "/admin_login",
        component: () =>
            import ("@/views/login/index"),
        hidden: true,
    },
    {
        path: "/agent/login",
        component: () =>
            import ("@/views/login/agentLogin"),
        hidden: true,
    },
    {
        path: "/platform/login",
        component: () =>
            import ("@/views/login/platformLogin"),
        hidden: true,
    },
    {
        path: "/sass/login",
        component: () =>
            import ("@/views/login/sassLogin"),
        hidden: true,
    },
    {
        path: "/sassSonAccount/login",
        component: () =>
            import ("@/views/login/sassSonAccount"),
        hidden: true,
    },
    {
        path: "/platformSonAccount/login",
        component: () =>
            import ("@/views/login/platformSonAccount"),
        hidden: true,
    },
    {
        path: "/401",
        name: "401",
        component: () =>
            import ("@/views/401"),
        hidden: true,
    },
    {
        path: "/loading",
        name: "loading",
        component: () =>
            import ("@/views/loading"),
        hidden: true,
    },
    {
        path: "/404",
        name: "404",
        component: () =>
            import ("@/views/404"),
        hidden: true,
    },
    {
        path: "/redirect",
        component: Layout,
        hidden: true,
        children: [{
            path: "/redirect/:path(.*)",
            component: () =>
                import ("@/views/redirect/index"),
        }, ],
    }
];

/*当settings.js里authentication配置的是intelligence时，views引入交给前端配置*/
export const asyncRoutes = [{
        path: "/",
        component: Layout,
        redirect: "/index",
        children: [{
            path: "/index",
            name: "Index",
            component: () =>
                import ("@/views/login/index"),
            meta: {
                title: "首页",
                icon: "home",
                affix: true,
                noKeepAlive: true,
            },
        }, ],
    },
    {
        path: "*",
        redirect: "/404",
        hidden: true,
    },
];

const router = new VueRouter({
    mode: routerMode,
    scrollBehavior: () => ({
        y: 0,
    }),
    routes: constantRoutes,
});
/* const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
}; */

export function resetRouter() {
    router.matcher = new VueRouter({
        mode: routerMode,
        scrollBehavior: () => ({
            y: 0,
        }),
        routes: constantRoutes,
    }).matcher;
}

export default router;
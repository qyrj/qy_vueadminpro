import request from "@/utils/request";
import { encryptedData } from "@/utils/encrypt";
import { loginRSA } from "@/config/settings";

// 管理员账号登录
export async function AdminLogin(data) {
    if (loginRSA) {
        data = await encryptedData(data);
    }
    return request({
        url: "/admin/login/login",
        method: "post",
        data,
    });
}

export function getInfo(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/user/user_info",
        method: "post",
        data
    });
}

export function logout() {
    return request({
        url: "/logout",
        method: "post",
    });
}
export function register() {
    return request({
        url: "/register",
        method: "post",
    });
}
export async function PlatformLogin(data) {
    if (loginRSA) {
        data = await encryptedData(data);
    }
    return request({
        url: "/thousands/platform.login/login",
        method: "post",
        data,
    });
}
// 平台-子账号登录
export function platformBypassAccountLogin(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.login/bypassAccountLogin",
        method: "POST",
        data,
    });
}
//sass-子账号登录
export function sassBypassAccountLogin(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.login/bypassAccountLogin",
        method: "POST",
        data,
    });
}
// sas账号登录
export function sassLogin(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.login/login",
        method: "POST",
        data,
    });
}
// 代理商账号登录
export async function AgentLogin(data) {
    if (loginRSA) {
        data = await encryptedData(data);
    }
    return request({
        url: "/sass/agent.login/login",
        method: "post",
        data,
    });
}
// 获取系统信息
export function getSystem(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/user.home/getSystem",
        method: "POST",
        data,
    });
}
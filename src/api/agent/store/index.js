import request from "@/utils/request";
// sass-平台列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.app/index",
        method: "POST",
        data,
    });
}
// sass-商家编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.app/edit",
        method: "POST",
        data,
    });
}
// sass-商家删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.app/delete",
        method: "POST",
        data,
    });
}
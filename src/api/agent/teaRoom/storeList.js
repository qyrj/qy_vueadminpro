import request from "@/utils/request";
// 茶室列表
export function StoreList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/user.tea_house/storeList",
        method: "POST",
        data,
    });
}
// 茶室房间列表
export function TeaHouseList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/user.tea_house/list",
        method: "POST",
        data,
    });
}
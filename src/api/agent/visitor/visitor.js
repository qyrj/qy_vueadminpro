import request from "@/utils/request";
// 访客列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.visitor_apply/index",
        method: "POST",
        data,
    });
}
// 访客核销
export function Handle(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.visitor_apply/handle",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 评测列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.evaluating/index",
        method: "POST",
        data,
    });
}
// 评测编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.evaluating/edit",
        method: "POST",
        data,
    });
}
// 评测删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.evaluating/delete",
        method: "POST",
        data,
    });
}
// 评测-申请列表
export function ApplyList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.evaluating/applyList",
        method: "POST",
        data,
    });
}
// 评测-申请处理
export function HandleApply(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.evaluating/handleApply",
        method: "POST",
        data,
    });
}
// 评测报告-列表
export function ReportList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.evaluating/reportList",
        method: "POST",
        data,
    });
}
// 评测报告-删除
export function EvaluatingDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.evaluating/evaluatingDel",
        method: "POST",
        data,
    });
}
// 评测报告-编辑
export function EvaluatingEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.evaluating/evaluatingEdit",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 平台-核销员列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.verifier/index",
        method: "POST",
        data,
    });
}
// 平台-核销员编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.verifier/edit",
        method: "POST",
        data,
    });
}
// 平台-核销员删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.verifier/delete",
        method: "POST",
        data,
    });
}
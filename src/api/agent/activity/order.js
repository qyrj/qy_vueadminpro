import request from "@/utils/request";
// 商户-活动订单列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.activity_order/index",
        method: "POST",
        data,
    });
}
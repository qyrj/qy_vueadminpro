import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.house_order/index",
        method: "POST",
        data,
    });
}
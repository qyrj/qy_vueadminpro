import request from "@/utils/request";
// 商户-数据预览
export function statistics(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/agent.index/statistics",
        method: "POST",
        data,
    });
}
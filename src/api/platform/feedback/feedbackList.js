import request from "@/utils/request";
// 商户-意见反馈列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.feedback/index",
        method: "POST",
        data,
    });
}
// 商户-意见反馈删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.feedback/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-用户列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/index",
        method: "POST",
        data,
    });
}
// 商户-用户编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/edit",
        method: "POST",
        data,
    });
}
// 商户-用户删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/delete",
        method: "POST",
        data,
    });
}
// 商户-快速编辑
export function QuickEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/quickEdit",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-获取系统信息
export function Info(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/info",
        method: "POST",
        data,
    });
}
// 商户-系统信息编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/edit",
        method: "POST",
        data,
    });
}
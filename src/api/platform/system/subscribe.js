import request from "@/utils/request";
// 信息
export function getList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.template_message/getList",
        method: "POST",
        data,
    });
}
// 编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.template_message/edit",
        method: "POST",
        data,
    });
}
// 编辑
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.template_message/index",
        method: "POST",
        data,
    });
}
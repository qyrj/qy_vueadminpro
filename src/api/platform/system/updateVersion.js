import request from "@/utils/request";
export function wxappUpload(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/wxappUpload",
        method: "POST",
        data,
    });
}
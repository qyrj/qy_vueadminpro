import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house/index",
        method: "POST",
        data,
    });
}
// 编辑
export function edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house/edit",
        method: "POST",
        data,
    });
}
//快速编辑
export function quickedit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house/quickedit",
        method: "POST",
        data,
    });
}
// 删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house/delete",
        method: "POST",
        data,
    });
}
// 标签列表
export function tags_list(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tags/index",
        method: "POST",
        data,
    });
}
// 分类列表
export function category_list(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.category/index",
        method: "POST",
        data,
    });
}
// 茶室状态列表
export function StatusList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house_stock/index",
        method: "POST",
        data,
    });
}
// 茶室状态编辑
export function EditStatus(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house_stock/edit",
        method: "POST",
        data,
    });
}
// 控电
export function electricity(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house_door/electricity",
        method: "POST",
        data,
    });
}
// 开门
export function openDoor(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house_door/openDoor",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.house_order/index",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 服务设施列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.facility/index",
        method: "POST",
        data,
    });
}
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.facility/edit",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.facility/delete",
        method: "POST",
        data,
    });
}
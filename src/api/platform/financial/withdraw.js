import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.withdraw/sassList",
        method: "POST",
        data,
    });
}
// 提现申请
export function apply(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.withdraw/sassApply",
        method: "POST",
        data,
    });
}
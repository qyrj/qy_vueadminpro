import request from "@/utils/request";
// 用户提现申请处理
export function Apply(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.withdraw/applyHandle",
        method: "POST",
        data,
    });
}
// 用户提现申请列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.withdraw/applyList",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-账单列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bill/index",
        method: "POST",
        data,
    });
}
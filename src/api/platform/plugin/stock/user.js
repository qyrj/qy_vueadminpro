import request from "@/utils/request";
// 商户-用户-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.user/index",
        method: "POST",
        data,
    });
}
// 商户-用户-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.user/edit",
        method: "POST",
        data,
    });
}
//商户-用户-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.user/delete",
        method: "POST",
        data,
    });
}
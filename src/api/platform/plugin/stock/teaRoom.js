import request from "@/utils/request";
// 商户-茶室-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.house/index",
        method: "POST",
        data,
    });
}
// 商户-茶室-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.house/edit",
        method: "POST",
        data,
    });
}
// 商户-茶室-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.house/delete",
        method: "POST",
        data,
    });
}
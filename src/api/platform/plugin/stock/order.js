import request from "@/utils/request";
// 商户-分红-订单-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.order/index",
        method: "POST",
        data,
    });
}
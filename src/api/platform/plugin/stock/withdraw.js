import request from "@/utils/request";
// 商户-提现-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.withdraw/index",
        method: "POST",
        data,
    });
}

// 商户-提现-处理
export function Handle(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.withdraw/handle",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-分红-订单-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.user/applyList",
        method: "POST",
        data,
    });
}
//商户-用户-申请处理
export function handleApply(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/plugin/stock.platform.user/handleApply",
        method: "POST",
        data,
    });
}
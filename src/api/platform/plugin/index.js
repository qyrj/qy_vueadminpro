import request from "@/utils/request";
// 列表
export function plugin(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/plugList",
        method: "POST",
        data,
    });
}
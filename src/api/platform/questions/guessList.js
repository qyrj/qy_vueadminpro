import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.guess/index",
        method: "POST",
        data,
    });
}
// 编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.guess/edit",
        method: "POST",
        data,
    });
}
// 删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.guess/delete",
        method: "POST",
        data,
    });
}
// 已参与
export function OrderList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.guess/orderList",
        method: "POST",
        data,
    });
}
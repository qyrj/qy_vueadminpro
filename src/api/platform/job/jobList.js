import request from "@/utils/request";
// 商户-活动列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.activity/index",
        method: "POST",
        data,
    });
}
// 商户-活动编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.activity/edit",
        method: "POST",
        data,
    });
}
// 商户-活动删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.activity/delete",
        method: "POST",
        data,
    });
}
// 商户-快捷编辑
export function QuickEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.activity/quickedit",
        method: "POST",
        data,
    });
}
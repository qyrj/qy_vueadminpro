import request from "@/utils/request";
// 商户-广告列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.ad/index",
        method: "POST",
        data,
    });
}
// 商户-广告编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.ad/edit",
        method: "POST",
        data,
    });
}
// 商户-广告删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.ad/delete",
        method: "POST",
        data,
    });
}
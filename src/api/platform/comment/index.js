import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tea_circle/index",
        method: "POST",
        data,
    });
}


export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tea_circle/edit",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tea_circle/delete",
        method: "POST",
        data,
    });
}
export function CommentList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tea_circle/commentList",
        method: "POST",
        data,
    });
}
export function commentDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.tea_circle/commentDel",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 消息列表
export function Index(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/thousands/platform.message/index",
        method: "POST",
        data
    });
}
// 消息删除
export function Delete(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/sass/user.message/delete",
        method: "POST",
        data
    });
}
// 消息
export function MessageList(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/thousands/platform.message/page",
        method: "POST",
        data
    });
}
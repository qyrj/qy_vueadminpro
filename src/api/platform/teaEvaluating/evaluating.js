import request from "@/utils/request";
// 商户-评测列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/index",
        method: "POST",
        data,
    });
}
// 商户-评测编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/edit",
        method: "POST",
        data,
    });
}
// 商户-评测删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/delete",
        method: "POST",
        data,
    });
}
// 商户-评测-申请列表
export function ApplyList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/applyList",
        method: "POST",
        data,
    });
}
// 商户-评测-申请处理
export function HandleApply(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/handleApply",
        method: "POST",
        data,
    });
}
// 商户-评测报告-列表
export function ReportList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/reportList",
        method: "POST",
        data,
    });
}
// 商户-评测报告-删除
export function EvaluatingDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/evaluatingDel",
        method: "POST",
        data,
    });
}
// 商户-评测报告-编辑
export function EvaluatingEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.evaluating/evaluatingEdit",
        method: "POST",
        data,
    });
}
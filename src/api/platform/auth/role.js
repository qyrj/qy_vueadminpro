import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.role/index",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.role/delete",
        method: "POST",
        data,
    });
}
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.role/edit",
        method: "POST",
        data,
    });
}
export function menuAuth(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.role/menuAuth",
        method: "POST",
        data,
    });
}
// 列表
export function bypassAccountIndex(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bypass_account/index",
        method: "POST",
        data,
    });
}
export function bypassAccountDelete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bypass_account/delete",
        method: "POST",
        data,
    });
}
export function bypassAccountEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bypass_account/edit",
        method: "POST",
        data,
    });
}
export function bypassAccountInfo(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bypass_account/info",
        method: "POST",
        data,
    });
}
export function bypassAccountInfoSave(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.bypass_account/infoSave",
        method: "POST",
        data,
    });
}
export function systemActionLog(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.system/ActionLog",
        method: "POST",
        data,
    });
}
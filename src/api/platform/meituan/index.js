import request from "@/utils/request";
// 城市列表
export function mtdianping(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/mtdianping",
        method: "POST",
        data,
    });
}
export function mtdianpingEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/mtdianpingEdit",
        method: "POST",
        data,
    });
}
export function CardCouponIndex(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/shared/platform.card_coupon/index",
        method: "POST",
        data,
    });
}
export function CardCouponEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/shared/platform.card_coupon/edit",
        method: "POST",
        data,
    });
}
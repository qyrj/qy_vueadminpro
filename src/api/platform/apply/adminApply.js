import request from "@/utils/request";
// 申请注册列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.staff/applyList",
        method: "POST",
        data,
    });
}
// 处理申请
export function applyHandle(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.staff/handle",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.staff/delete",
        method: "POST",
        data,
    });
}
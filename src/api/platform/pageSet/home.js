import request from "@/utils/request";
// 首页导航
export function wxappTabBar(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/wxappTabBar",
        method: "POST",
        data,
    });
}
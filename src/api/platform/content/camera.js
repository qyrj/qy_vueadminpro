import request from "@/utils/request";
//视频监控-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.cctv/index",
        method: "POST",
        data,
    });
}
//视频监控-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.cctv/delete",
        method: "POST",
        data,
    });
}
//视频监控-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.cctv/edit",
        method: "POST",
        data,
    });
}
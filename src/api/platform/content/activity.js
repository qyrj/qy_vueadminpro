import request from "@/utils/request";
//农场活动-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.farm_activity/index",
        method: "POST",
        data,
    });
}
//农场活动-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.farm_activity/delete",
        method: "POST",
        data,
    });
}
//农场活动-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.farm_activity/edit",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-商家分类列表
export function StoreCategoryIndex(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store_category/index",
        method: "POST",
        data,
    });
}
// 商户-商家分类编辑
export function StoreCategoryEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store_category/edit",
        method: "POST",
        data,
    });
}
// 商户-商家分类删除
export function StoreCategoryDelete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store_category/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-商家列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store/index",
        method: "POST",
        data,
    });
}
// 商户-商家编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store/edit",
        method: "POST",
        data,
    });
}
// 商户-商家删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store/delete",
        method: "POST",
        data,
    });
}
// 商户-商家是否首页推荐
export function Quickedit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.store/quickedit",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 商户-商家信息设置
export function otherSetting(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.settings/otherSetting",
        method: "POST",
        data,
    });
}
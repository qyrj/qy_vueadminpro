import request from "@/utils/request";
// 获取用户列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/index",
        method: "POST",
        data,
    });
}
//充值
export function recharge(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/recharge",
        method: "POST",
        data,
    });
}
//充值记录
export function rechargeLog(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/rechargeLog",
        method: "POST",
        data,
    });
}
//浏览记录
export function pageView(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.index/uvList",
        method: "POST",
        data,
    });
}
//编辑
export function quickEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/quickEdit",
        method: "POST",
        data,
    });
}
//编辑
export function PlatformUser(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.user/platformUser",
        method: "POST",
        data,
    });
}
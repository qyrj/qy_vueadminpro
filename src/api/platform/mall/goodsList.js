import request from "@/utils/request";
// 商品列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.goods/index",
        method: "POST",
        data,
    });
}
// 编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.goods/edit",
        method: "POST",
        data,
    });
}
// 快速编辑
export function quickedit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.goods/quickedit",
        method: "POST",
        data,
    });
}
// 删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.goods/delete",
        method: "POST",
        data,
    });
}
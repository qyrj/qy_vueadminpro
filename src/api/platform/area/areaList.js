import request from "@/utils/request";
// 商户-区域列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.area/index",
        method: "POST",
        data,
    });
}
// 商户-区域编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.area/edit",
        method: "POST",
        data,
    });
}
// 商户-区域删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/platform.area/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// sass-数据预览
export function statistics(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.index/statistics",
        method: "POST",
        data,
    });
}
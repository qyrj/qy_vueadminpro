import request from "@/utils/request";
// 日志
export function logList(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/sass/platform.index/systemLog",
        method: "POST",
        data
    });
}
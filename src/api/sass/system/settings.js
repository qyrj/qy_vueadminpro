import request from "@/utils/request";
// 平台-获取系统信息
export function Info(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/info",
        method: "POST",
        data,
    });
}
// 平台-系统信息编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/edit",
        method: "POST",
        data,
    });
}
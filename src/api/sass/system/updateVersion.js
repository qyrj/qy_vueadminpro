import request from "@/utils/request";
export function wxappUpload(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/wxappUpload",
        method: "POST",
        data,
    });
}
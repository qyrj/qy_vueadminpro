import request from "@/utils/request";
// 消息列表
export function Index(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/sass/platform.message/index",
        method: "POST",
        data
    });
}
// 消息删除
export function Delete(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/sass/platform.message/delete",
        method: "POST",
        data
    });
}
// 消息推送
export function PushMsg(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/sass/platform.message/pushMsg",
        method: "POST",
        data
    });
}
import request from "@/utils/request";
// 优惠券-列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.coupon/index",
        method: "POST",
        data,
    });
};
// 优惠券-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.coupon/edit",
        method: "POST",
        data,
    });
};
// 优惠券-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.coupon/delete",
        method: "POST",
        data,
    });
};
//发放优惠券
export function Distribution(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.coupon/distribution",
        method: "POST",
        data,
    });
}
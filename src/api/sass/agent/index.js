import request from "@/utils/request";
// sass-代理商列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/index",
        method: "POST",
        data,
    });
}
// sass-代理商编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/edit",
        method: "POST",
        data,
    });
}
// sass-代理商删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 申请注册列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/applyList",
        method: "POST",
        data,
    });
}
// 处理申请
export function applyHandle(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/applyHandle",
        method: "POST",
        data,
    });
}
// 申请删除
export function applyDelte(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.agent/applyDelte",
        method: "POST",
        data,
    });
}
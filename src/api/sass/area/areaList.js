import request from "@/utils/request";
// 平台-区域列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/districtList",
        method: "POST",
        data,
    });
}
// 平台-区域编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/districtEdit",
        method: "POST",
        data,
    });
}
// 平台-区域删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.area/delete",
        method: "POST",
        data,
    });
}
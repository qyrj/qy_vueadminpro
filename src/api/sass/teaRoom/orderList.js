import request from "@/utils/request";
// 茶室订单列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.tea_house_order/index",
        method: "POST",
        data,
    });
}
// 茶室订单提前结束
export function cancelOrder(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.tea_house_order/endOrdersEarly",
        method: "POST",
        data,
    });
}
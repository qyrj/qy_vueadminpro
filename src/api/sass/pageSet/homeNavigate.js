import request from "@/utils/request";
// 首页导航
export function homeNav(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/homeNav",
        method: "POST",
        data,
    });
}
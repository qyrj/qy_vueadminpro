import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/typeList",
        method: "POST",
        data,
    });
}
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/typeEdit",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/typeDelete",
        method: "POST",
        data,
    });
}
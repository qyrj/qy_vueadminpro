import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/index",
        method: "POST",
        data,
    });
}


export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/edit",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/delete",
        method: "POST",
        data,
    });
}
export function CommentList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/commentList",
        method: "POST",
        data,
    });
}
export function commentDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.school/commentDel",
        method: "POST",
        data,
    });
}
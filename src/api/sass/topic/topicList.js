import request from "@/utils/request";
// 话题列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.circle_topic/index",
        method: "POST",
        data,
    });
}
// 编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.circle_topic/edit",
        method: "POST",
        data,
    });
}
// 话题删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.circle_topic/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// sass-获取用户列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.user/index",
        method: "POST",
        data,
    });
}
//sass-充值
export function recharge(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.user/recharge",
        method: "POST",
        data,
    });
}
//sass-充值记录
export function rechargeLog(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.user/rechargeLog",
        method: "POST",
        data,
    });
}
//sass-浏览记录
export function pageView(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.index/uvList",
        method: "POST",
        data,
    });
}
//sass-编辑
export function quickEdit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.user/quickEdit",
        method: "POST",
        data,
    });
}
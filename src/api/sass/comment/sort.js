import request from "@/utils/request";
export function contentSetting(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/contentSetting",
        method: "POST",
        data,
    });
}
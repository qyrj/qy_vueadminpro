import request from "@/utils/request";
//分销设置
export function share_setting(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/shareSetting",
        method: "POST",
        data,
    });
}
// 分销规则设置
export function shareRuleSetting(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.settings/shareRuleSetting",
        method: "POST",
        data,
    });
}
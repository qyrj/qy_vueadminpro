import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.exercise/index",
        method: "POST",
        data,
    });
}
// 编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.exercise/edit",
        method: "POST",
        data,
    });
}
// 已参与
export function OrderList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.exercise/orderList",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 问答列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.questions/index",
        method: "POST",
        data,
    });
}
// 评论删除
export function CommentDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.questions/commentDel",
        method: "POST",
        data,
    });
}
// 评论列表
export function CommentList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.questions/commentList",
        method: "POST",
        data,
    });
}
// 问答编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.questions/edit",
        method: "POST",
        data,
    });
}
// 问答删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.questions/delete",
        method: "POST",
        data,
    });
}
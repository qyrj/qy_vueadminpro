import request from "@/utils/request";
// sass-商品分类列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_cat/index",
        method: "POST",
        data,
    });
}
// sass-分类编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_cat/edit",
        method: "POST",
        data,
    });
}
// sass-分类删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_cat/delete",
        method: "POST",
        data,
    });
}
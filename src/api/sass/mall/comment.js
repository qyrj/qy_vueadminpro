import request from "@/utils/request";
// 商品-评论-列表
export function CommentList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/commentList",
        method: "POST",
        data,
    });
}
// 评论-删除
export function CommentDelete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/commentDelete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// sass-商品列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/index",
        method: "POST",
        data,
    });
}
// sass-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/edit",
        method: "POST",
        data,
    });
}
// sass-快速编辑
export function quickedit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/quickedit",
        method: "POST",
        data,
    });
}
// sass-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods/delete",
        method: "POST",
        data,
    });
}
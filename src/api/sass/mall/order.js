import request from "@/utils/request";
// sass-商城-订单
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_order/index",
        method: "POST",
        data,
    });
}
// sass-商城-订单-发货
export function sendOrder(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_order/sendOrder",
        method: "POST",
        data,
    });
}
// sass-商城-订单-完成
export function Complete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_order/complete",
        method: "POST",
        data,
    });
}
// sass-商城-订单-取消
export function Cancel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.goods_order/cancel",
        method: "POST",
        data,
    });
}
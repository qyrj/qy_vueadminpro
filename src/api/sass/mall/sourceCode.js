import request from "@/utils/request";
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.material_qrcode/index",
        method: "POST",
        data,
    });
}


export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.material_qrcode/edit",
        method: "POST",
        data,
    });
}
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.material_qrcode/delete",
        method: "POST",
        data,
    });
}
export function userUv(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.index/userUv",
        method: "POST",
        data,
    });
}
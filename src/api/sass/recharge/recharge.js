import request from "@/utils/request";
// 平台-充值列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.recharge/index",
        method: "POST",
        data,
    });
}
// 平台-充值-编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.recharge/edit",
        method: "POST",
        data,
    });
}
// 平台-充值-删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.recharge/delete",
        method: "POST",
        data,
    });
}
export function orderList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.recharge/orderList",
        method: "POST",
        data,
    });
}
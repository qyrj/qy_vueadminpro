import request from "@/utils/request";
// 申请注册列表
// export function Index(data) {
//     data.access_token = localStorage.getItem("access_token");
//     return request({
//         url: "/sass/platform.app/applyList",
//         method: "POST",
//         data,
//     });
// }
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/applyPlatformList",
        method: "POST",
        data,
    });
}

// 处理申请
export function regist_result(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/applyHandle",
        method: "POST",
        data,
    });
}

// 申请删除
export function applyPlatformDel(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/applyPlatformDel",
        method: "POST",
        data,
    });
}
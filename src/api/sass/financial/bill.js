import request from "@/utils/request";
// sass-账单列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.bill/index",
        method: "POST",
        data,
    });
}
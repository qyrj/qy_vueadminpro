import request from "@/utils/request";
// 列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.withdraw/index",
        method: "POST",
        data,
    });
}
// 提现处理
export function handle(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.withdraw/handle",
        method: "POST",
        data,
    });
}
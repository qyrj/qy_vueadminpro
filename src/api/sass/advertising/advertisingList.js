import request from "@/utils/request";
// 平台-广告列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.ad/index",
        method: "POST",
        data,
    });
}
// 平台-广告编辑
export function Edit(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.ad/edit",
        method: "POST",
        data,
    });
}
// 平台-广告删除
export function Delete(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.ad/delete",
        method: "POST",
        data,
    });
}
import request from "@/utils/request";
// 平台-空间入驻申请列表
export function Index(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/sass/platform.app/applyPlatformList",
        method: "POST",
        data,
    });
}
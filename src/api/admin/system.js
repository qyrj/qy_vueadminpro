import request from "@/utils/request";
// 日志
export function logList(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/shared/platform.index/systemLog",
        method: "POST",
        data
    });
}
// 更新
export function updata(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/system/updata",
        method: "POST",
        data
    });
}
// 更新
export function uploadInfo(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/system/uploadInfo",
        method: "POST",
        data
    });
}
// 上传设置
export function uploadSetting(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/system/uploadSetting",
        method: "POST",
        data
    });
}
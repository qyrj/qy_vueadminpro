import request from "@/utils/request";
import settings from "@/config/settings";
let baseURL = process.env.NODE_ENV !== "production" ? settings.baseURL : window.location.origin + '/index.php?s=';
// 上传接口
export function UpLoad(category_id = '') {
    // let url = settings.baseURL + "/admin/attachment/upload&access_token=" + localStorage.getItem('access_token') + "&category_id=" + category_id;
    let url = baseURL + "/admin/attachment/upload&access_token=" + localStorage.getItem('access_token') + "&category_id=" + category_id;
    return url;
}
// 添加/编辑
export function Category(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/category",
        method: "post",
        data
    });
}
export function CategoryEdit(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/category_edit",
        method: "post",
        data
    });
}
export function CategoryDelete(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/category_delete",
        method: "post",
        data
    });
}
export function FileList(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/file_list",
        method: "post",
        data
    });
}
export function FileDelete(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/file_delete",
        method: "post",
        data
    });
}
export function FileMove(data) {
    data.access_token = localStorage.getItem('access_token');
    return request({
        url: "/admin/attachment/file_move",
        method: "post",
        data
    });
}
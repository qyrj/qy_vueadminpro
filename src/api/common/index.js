import request from "@/utils/request";
// 商户-省市区
export function district(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/common/district",
        method: "POST",
        data,
    });
}
// 商户-街道-商圈
export function areaList(data) {
    data.access_token = localStorage.getItem("access_token");
    return request({
        url: "/thousands/common/areaList",
        method: "POST",
        data,
    });
}